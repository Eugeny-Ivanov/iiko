<?php

namespace src;

class Html
{
    /**
     * Дерево групп
     * @param array $menuData
     * @param integer $parent_id
     * @return string
     */
    public function dataHtmlTree($menuData, $parent_id = null)
    {
        $html = '';
        foreach ($menuData['groups'] as $row) {
            if ($row['parentGroup'] == $parent_id) {
                $html .= '<li>' . "\n";
                $html .= $row['name'] . "\n";
                $html .= $this->productsListHtml($menuData['products'], $row['id']);
                $html .= $this->dataHtmlTree($menuData, $row['id']);
                $html .= '</li>' . "\n";
            }
        }
        return $html ? '<ul>' . $html . '</ul>' . "\n" : '';
    }

    /**
     * Таблица продуктов
     * @param array $list
     * @param null $groupId
     * @return string
     */
    public function productsListHtml($list, $groupId = null)
    {
        $list = array_filter($list, function ($el) use ($groupId) {
            return $el['parentGroup'] == $groupId;
        });
        if (!$list) return '';
        $thead = array_keys(reset($list));
        $theadHtml = array_map(function ($el) {
            return '<th scope="col">' . $el . '</th>';
        }, $thead);
        $tbodyList = array_map([$this, 'productVals'], $list);
        $tbodyHtml = array_map(function ($el) {
            return '<tr>'.join("\n", $el).'</tr>';
        }, $tbodyList);
        return $tbodyHtml ?
            '<a class="btn btn-link" data-toggle="collapse" 
            href="#collapseExample'.$groupId.'" 
            role="button" aria-expanded="false" aria-controls="collapseExample">Продукты</a>
            <div class="collapse" id="collapseExample'.$groupId.'">
            <table class="table"><thead><tr>'
            . join("\n", $theadHtml)
            . '</tr></thead><tbody><tr>'
            . join("\n", $tbodyHtml)
            . '</tr></tbody></table></div>' : '';

    }

    /**
     * Строка таблицы продуктов
     * @param array $el
     * @return string[]
     */
    private function productVals($el)
    {
        return array_map(function ($val) {
            if (empty($val)) {
                return '<td>not set</td>';
            } elseif (is_array($val)) {
                return '<td>array</td>';
            } elseif (is_string($val)) {
                return '<td>' . $val . '</td>';
            } else {
                return '<td>unknown type</td>';
            }
        }, $el);
    }
}
