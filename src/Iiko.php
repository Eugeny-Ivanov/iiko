<?php


namespace src;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Iiko extends Client
{
    private $login = null;
    private $password = null;
    private $token_file = __DIR__ . '/../token.json';
    private $baseUrl = 'https://iiko.biz:9900/api/0/';

    function __construct($login = null, $password = null)
    {
        $this->login = $login;
        $this->password = $password;
        parent::__construct([
            'base_uri' => $this->baseUrl,
            'timeout' => 2.0,
        ]);
    }

    /**
     * Запрос токена доступа
     * @param int $counter
     * @return false|int
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function requestToken($counter = 0) : bool
    {
        $query = ['query' => [
            'user_id' => $this->login,
            'user_secret' => $this->password,
        ]];
        try {
            $response = $this->get('auth/access_token', $query);
            $token = $response->getBody()->getContents();
            //Странно, возвращает токен внутри "", для мобильного приложения похоже так надо.
            $token = preg_replace('/(")/u', null, $token);
            return $token ? (boolean)file_put_contents($this->token_file, json_encode(['access_token' => $token])) : false;
        } catch (ClientException $e) {
            //Повторим пару раз для надежности
            if ($counter < 2) {
                $counter++;
                $this->requestToken($counter);
            } else {
                //Если с 2 раз ничего не вышло
                throw new \Exception($e->getResponse()->getReasonPhrase(), $e->getResponse()->getStatusCode());
            }
        }
    }

    /**
     * Получение токена из файла, проверка и обновление если нужно
     * @return false|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getToken()
    {
        $tokenArray = is_file($this->token_file) ? json_decode(file_get_contents($this->token_file), 1) : null;
        //Если токена нет в файле или файла нет
        if (empty($tokenArray['access_token'])) {
            $this->requestToken();
            $tokenArray = json_decode(file_get_contents($this->token_file), 1);
        } else {
            //Проверка токена на актуальность
            try {
                $response = $this->get('auth/echo', [
                    'query' => [
                        'access_token' => !empty($tokenArray['access_token']) ? $tokenArray['access_token'] : null,
                        'msg' => 'wrong'
                    ]
                ]);
                //Почему то msg не возвращает, возвращает дефолтное значение
                if ($response->getBody()->getContents() == '"Wrong access token"') {
                    $this->requestToken();
                    $tokenArray = json_decode(file_get_contents($this->token_file), 1);
                }
            } catch (ClientException $e) {
                throw new \Exception($e->getResponse()->getReasonPhrase(), $e->getResponse()->getStatusCode());
            }
        }
        return empty($tokenArray['access_token']) ? null : $tokenArray['access_token'];
    }

    /**
     * Запрос номенклатуры
     * @param int $organizationId
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMenu($organizationId)
    {
        $token = $this->getToken();

        if (!$token) return [];
        $query = [
            'query' => [
                'access_token' => $token,
            ]
        ];
        try {
            $response = $this->get("nomenclature/{$organizationId}", $query);
            return json_decode($response->getBody()->getContents(), 1);
        } catch (ClientException $e) {
           throw new \Exception($e->getResponse()->getReasonPhrase(), $e->getResponse()->getStatusCode());
        }
    }

    /**
     * Запрос списка организаций
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getOrganisations(){
        $query = [
            'query' => [
                'access_token' => $this->getToken(),
            ],
        ];
        try {
            $response = $this->get('organization/list', $query);
            return json_decode($response->getBody()->getContents(), 1);
        } catch (ClientException $e) {
            throw new \Exception($e->getResponse()->getReasonPhrase(), $e->getResponse()->getStatusCode());
        }
    }
}
